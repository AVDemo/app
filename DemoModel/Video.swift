//
//  Video.swift
//  AVDemo
//
//  Created by Filip Klembara on 15/08/2020.
//

import Foundation
import AVFoundation
import UIKit

public class Video: ObservableObject, Identifiable {
    private let asset: AVAsset
    
    public let name: String
    public let url: URL
    
    @Published
    public var sections: [VideoSection] = []
    
    @Published
    public var preview: UIImage
    
    public init(name: String, url: URL) {
        self.name = name
        self.url = url
        asset = AVURLAsset(url: url)
        
        // preview default init
        preview = UIImage(systemName: "person")!
        // load real preview
        asyncPreviewInit()
    }
    
    private func asyncPreviewInit() {
        DispatchQueue.global(qos: .userInitiated).async {
            let generator = AVAssetImageGenerator(asset: self.asset)
            generator.appliesPreferredTrackTransform = true
            let time = CMTime(seconds: 1, preferredTimescale: 1)
            guard let image = try? generator.copyCGImage(at: time, actualTime: nil) else {
                return
            }
            DispatchQueue.main.async {
                self.preview = UIImage(cgImage: image)
            }
        }
    }
    
    public var durationSeconds: Double {
        let r = asset.duration.seconds
        return r.isNaN ? 0 : r
    }
    
    /// Duration of video
    public var duration: String {
        Video.durationDescription(from: durationSeconds)
    }
    
    public var id: String {
        return name.lowercased()
    }
}
// MARK: - Duration
extension Video {
    private static func normalize(_ i: Int) -> String {
        i < 10 ? "0\(i)" : i.description
    }
    
    static func durationDescription(from duration: CMTime) -> String {
        let b = duration.seconds
        return durationDescription(from: b)
    }
    
    static func durationDescription(from seconds: Double, floorValue: Bool = true) -> String {
        let a = seconds.isNaN ? 0 : seconds
        // floor
        let d = floorValue ? round(a + 0.49) : a
        let durationInSeconds = Int(d)
        let hours = durationInSeconds / 3600
        let minutes = (durationInSeconds % 3600) / 60
        let seconds = (durationInSeconds % 3600) % 60
        let h = hours > 0 ? "\(hours):" : ""
        
        return "\(h)\(normalize(minutes)):\(normalize(seconds))"
    }
}
