//
//  VideoPlayer.swift
//  DemoModel
//
//  Created by Filip Klembara on 15/08/2020.
//

import AVFoundation
import Combine

public class VideoPlayer: ObservableObject {
    private var videoUpdatePublisher: Cancellable?
    
    /// Periodic observer token
    private var token: Any?
    
    public let video: Video
    
    private let playerItem: AVPlayerItem
    private var playerLooper: AVPlayerLooper
    
    public let player = AVQueuePlayer()
    
    @Published
    public var selectedSection: VideoSection? {
        didSet {
            guard oldValue != selectedSection else { return }
            let wasPlaying = isPlaying
            isPlaying = false
            playerLooper.disableLooping()
            if let selected = selectedSection {
                let dur = player.currentItem!.asset.duration.seconds
                let start = CMTime(seconds: dur * selected.start, preferredTimescale: 1000)
                let end = CMTime(seconds: dur * selected.end, preferredTimescale: 1000)
                playerLooper = .init(player: player, templateItem: playerItem, timeRange: .init(start: start, end: end))
                currentPosition = selected.start
            } else {
                playerLooper = .init(player: player, templateItem: playerItem)
            }
            isPlaying = wasPlaying
        }
    }
    
    /// Current position [%]
    @Published
    public var currentPosition = 0.0
    
    public var currentTime: String {
        let k = player.currentTime().seconds
        return Video.durationDescription(from: k, floorValue: false)
    }
    
    @Published
    public var isPlaying = false {
        didSet {
            if isPlaying {
                player.play()
                addObserver()
            } else {
                player.pause()
                removeObserver()
            }
        }
    }
    
    public init(video: Video) {
        self.video = video
        playerItem = AVPlayerItem(url: video.url)
        playerLooper = AVPlayerLooper(player: player, templateItem: playerItem)
        videoUpdatePublisher = video.objectWillChange.sink(receiveValue: resend)
    }
    
    /// Send objectWillChange
    private func resend() {
        objectWillChange.send()
        DispatchQueue.main.async {
            if self.selectedSection == nil, let s = self.video.sections.first {
                self.selectedSection = s
            }
        }
    }
    
    deinit {
        removeObserver()
    }
}

// MARK: - Add/Remove observer
extension VideoPlayer {
    private func removeObserver() {
        guard let t = token else { return }
        player.removeTimeObserver(t)
        token = nil
    }
    
    private func addObserver() {
        if token != nil {
            removeObserver()
        }
        token = player.addPeriodicTimeObserver(forInterval: .init(seconds: 0.03, preferredTimescale: 100), queue: nil) { [unowned self] t in
            let a1 = self.player.currentTime().seconds
            let a2 = self.player.currentItem!.asset.duration.seconds
            let a = a1 / a2
        
            guard !a.isNaN else { return }
            self.currentPosition = a
        }
    }
}
