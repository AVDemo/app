//
//  ServerBeckend.swift
//  DemoModel
//
//  Created by Filip Klembara on 15/08/2020.
//

import Foundation

public struct ServerBackend {
    public init() { }
    
    /// Download sections for video
    /// - Parameter video: video
    public func downloadSections(for video: Video) {
        guard let id = VideoDatabase.VideoName(rawValue: video.id) else {
            return
        }
        let serverResponse: [(Double, Double)]
        switch id {
        case .beach:
            serverResponse = [(0, 0.1), (0.2, 0.5), (0.6, 0.8)]
        case .street:
            serverResponse = [(0.2, 0.5)]
        case .ballet:
            serverResponse = [(0.3, 0.4), (0.45, 0.6), (0.7, 1)]
        }
        DispatchQueue.main.async {
            video.sections = serverResponse.map { VideoSection(start: $0, end: $1) }
        }
    }
}
