//
//  VideoSection.swift
//  DemoModel
//
//  Created by Filip Klembara on 15/08/2020.
//

import Foundation

/// Video section
public struct VideoSection {
    /// Init video section with given start and end time in %
    /// - Parameters:
    ///   - start: Video section start
    ///   - end: Video section end
    public init(start: Double, end: Double) {
        self.start = start
        self.end = end
    }
    
    public let start: Double
    
    public let end: Double
}

// MARK: - Identifiable
extension VideoSection: Identifiable {
    public var id: String {
        "\(start)-\(end)"
    }
}

extension VideoSection: Equatable { }
