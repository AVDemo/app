//
//  VideoDatabase.swift
//  AVDemo
//
//  Created by Filip Klembara on 15/08/2020.
//

import Foundation
import Combine

public class VideoDatabase: ObservableObject {
    private var bag = Set<AnyCancellable>()
    
    private init() {
        videos = VideoName.allCases.map(VideoDatabase.video(for:))
        videos.forEach { $0.objectWillChange.sink(receiveValue: resend).store(in: &bag) }
    }
    
    private func resend() {
        objectWillChange.send()
    }
    
    public static let shared = VideoDatabase()
    
    @Published
    public var videos: [Video]
}

// MARK: - Video init
extension VideoDatabase {
    enum VideoName: String, CaseIterable {
        case beach
        case street
        case ballet
    }
    
    private static func url(for video: VideoName) -> URL {
        guard let path = Bundle.main.path(forResource: video.rawValue, ofType: "mp4") else {
            fatalError("Missing video with name: \(video.rawValue)")
        }
        return URL(fileURLWithPath: path)
    }
    
    private static func video(for name: VideoName) -> Video {
        Video(name: name.rawValue.capitalized, url: url(for: name))
    }
    
}
