//
//  DemoModelTests.swift
//  DemoModelTests
//
//  Created by Filip Klembara on 15/08/2020.
//

import XCTest
@testable import DemoModel

class DemoModelTests: XCTestCase {
    func testDuration() throws {
        let a = Video.durationDescription(from: 3600 * 4 + 21 * 60 + 19)
        XCTAssertEqual(a, "4:21:19")
        let b = Video.durationDescription(from: 3600 * 4 + 21 * 60 + 19.3)
        XCTAssertEqual(b, "4:21:20")
        let c = Video.durationDescription(from: 3600 * 4 + 19.3)
        XCTAssertEqual(c, "4:00:20")
        let d = Video.durationDescription(from: 19.3)
        XCTAssertEqual(d, "00:20")
        let e = Video.durationDescription(from: 22 * 60 + 19.3)
        XCTAssertEqual(e, "22:20")
    }
    
    func testDurationNoFloor() throws {
        let a = Video.durationDescription(from: 3600 * 4 + 21 * 60 + 19, floorValue: false)
        XCTAssertEqual(a, "4:21:19")
        let b = Video.durationDescription(from: 3600 * 4 + 21 * 60 + 19.3, floorValue: false)
        XCTAssertEqual(b, "4:21:19")
        let c = Video.durationDescription(from: 3600 * 4 + 19.3, floorValue: false)
        XCTAssertEqual(c, "4:00:19")
        let d = Video.durationDescription(from: 19.3, floorValue: false)
        XCTAssertEqual(d, "00:19")
        let e = Video.durationDescription(from: 22 * 60 + 19.3, floorValue: false)
        XCTAssertEqual(e, "22:19")
    }
}
