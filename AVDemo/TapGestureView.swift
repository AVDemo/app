//
//  TapGestureView.swift
//  AVDemo
//
//  Created by Filip Klembara on 15/08/2020.
//

import SwiftUI

struct TapGestureView: UIViewRepresentable {
    typealias Action = (CGPoint) -> Void
    let action: Action
    
    func makeUIView(context: UIViewRepresentableContext<TapGestureView>) -> UIView {
        let view = UIView(frame: .zero)
        let gesture = UITapGestureRecognizer(
            target: context.coordinator,
            action: #selector(Coordinator.tap))
        view.addGestureRecognizer(gesture)
        return view
    }
    
    class Coordinator: NSObject {
        let action: Action
        
        init(action: @escaping Action) {
            self.action = action
        }
        
        @objc func tap(gesture:UITapGestureRecognizer) {
            let point = gesture.location(in: gesture.view)
            action(point)
        }
    }
    
    func makeCoordinator() -> TapGestureView.Coordinator {
        return Coordinator(action: self.action)
    }
    
    func updateUIView(_ uiView: UIView,
                      context: UIViewRepresentableContext<TapGestureView>) { }
}

struct TapGestureView_Previews: PreviewProvider {
    static var previews: some View {
        TapGestureView { _ in }
    }
}
