//
//  VideoDetail.swift
//  AVDemo
//
//  Created by Filip Klembara on 15/08/2020.
//

import SwiftUI
import DemoModel

struct VideoDetail: View {
    @ObservedObject
    var player: VideoPlayer
    
    init(video: Video) {
        player = VideoPlayer(video: video)
    }
    
    private var buttonName: String {
        player.isPlaying ? "stop" : "play"
    }
    
    var body: some View {
        VStack {
            PlayerView(player: player.player)
            HStack {
                // play/stop button
                Button(action: togglePlay) {
                    Image(systemName: buttonName)
                        .resizable()
                        .foregroundColor(.white)
                        .frame(width: 15, height: 15)
                        .padding(4)
                }.background(RoundedRectangle(cornerRadius: 5).foregroundColor(.secondary))
                // Video progress bar
                VideoProgressView(player: player)
                    .frame(maxHeight: 40)
                // Current position
                Text("\(player.currentTime)/\(player.video.duration)")
                    .font(.system(.footnote, design: .monospaced))
            }
            .padding(.horizontal, 5)
        }.onAppear {
            self.player.isPlaying = true
        }.onDisappear {
            self.player.isPlaying = false
        }.navigationBarTitle(player.video.name)
    }
    
    private func togglePlay() {
        player.isPlaying.toggle()
    }
}

struct VideoDetail_Previews: PreviewProvider {
    static var previews: some View {
        VideoDetail(video: VideoDatabase.shared.videos.first!)
    }
}
