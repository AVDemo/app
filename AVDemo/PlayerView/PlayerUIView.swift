//
//  PlayerUIView.swift
//  AVDemo
//
//  Created by Filip Klembara on 15/08/2020.
//

import UIKit
import AVFoundation

class PlayerUIView: UIView {
    
    private let playerLayer = AVPlayerLayer()
    private let player: AVPlayer
    
    init(player: AVPlayer) {
        self.player = player
        super.init(frame: .zero)
        
        playerLayer.player = player
        layer.addSublayer(playerLayer)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        playerLayer.frame = bounds
    }
    
}
