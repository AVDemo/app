//
//  PlayerView.swift
//  AVDemo
//
//  Created by Filip Klembara on 15/08/2020.
//

import SwiftUI
import AVFoundation
import DemoModel

struct PlayerView: UIViewRepresentable {
    let player: AVPlayer
    
    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<PlayerView>) {
    }
    
    func makeUIView(context: Context) -> UIView {
        return PlayerUIView(player: player)
    }
}

struct PlayerView_Previews: PreviewProvider {
    static var previews: some View {
        let video = VideoDatabase.shared.videos.first!
        let p = AVPlayer(url: video.url)
        return PlayerView(player: p)
    }
}
