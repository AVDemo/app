//
//  VideoRow.swift
//  AVDemo
//
//  Created by Filip Klembara on 15/08/2020.
//

import SwiftUI
import DemoModel

struct VideoRow: View {
    @ObservedObject
    var video: Video
    
    private var sections: String {
        if video.sections.isEmpty {
            return "(no loaded sections)"
        } else {
            return "(sections: \(video.sections.count))"
        }
    }
    
    var body: some View {
        HStack {
            Image(uiImage: video.preview)
                .resizable()
                .scaledToFit()
                .frame(width: 100, height: 60)
            VStack {
                Text(video.name)
                Text(video.duration)
            }
            Spacer()
            Text(sections)
                .font(.subheadline)
                .foregroundColor(.secondary)
        }
    }
}

struct VideoRow_Previews: PreviewProvider {
    static var previews: some View {
        VideoRow(video: VideoDatabase.shared.videos.first!)
            .previewLayout(.fixed(width: 400, height: 80))
    }
}
