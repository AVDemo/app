//
//  VideoListView.swift
//  AVDemo
//
//  Created by Filip Klembara on 15/08/2020.
//

import SwiftUI
import DemoModel

struct VideoListView: View {
    private let database = VideoDatabase.shared
    
    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            NavigationView {
                List {
                    ForEach(database.videos) { video in
                        NavigationLink(destination: VideoDetail(video: video)) {
                            VideoRow(video: video)
                        }
                    }
                    // Request buttons
                    Section(header: Text("Request sections")) {
                        RequestButton(title: "Now") {
                            self.request()
                        }
                        RequestButton(title: "After 5 seconds") {
                            self.request(after: 5)
                        }
                    }
                }.navigationBarTitle("Videos")
            }
            .navigationViewStyle(StackNavigationViewStyle())
        }
    }
    
    private func request(after seconds: Double = 0) {
        DispatchQueue.global().asyncAfter(deadline: .now() + seconds) {
            self.database.videos.forEach { v in
                ServerBackend().downloadSections(for: v)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        VideoListView()
    }
}

struct RequestButton: View {
    let title: String
    let action: () -> Void
    
    var body: some View {
        HStack {
            Spacer()
            Button(action: action) {
                Text(title)
                    .foregroundColor(.white)
                    .padding(8)
            }
            .background(RoundedRectangle(cornerRadius: 10).fill(Color.blue))
            Spacer()
        }.padding(5)
    }
}
