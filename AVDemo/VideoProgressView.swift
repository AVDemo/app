//
//  VideoProgressView.swift
//  AVDemo
//
//  Created by Filip Klembara on 15/08/2020.
//

import SwiftUI
import DemoModel

struct VideoProgressView: View {
    @ObservedObject
    var player: VideoPlayer
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .leading) {
                // video
                RoundedRectangle(cornerRadius: 3)
                    .stroke()
                    .fill(Color.secondary)
                    .frame(height: 16)
                // video sections
                ForEach(self.player.video.sections) { section in
                    RoundedRectangle(cornerRadius: 3)
                        .fill(section == self.player.selectedSection ? Color.green : Color.gray)
                        .frame(width: section.width(for: geometry), height: 10)
                        .offset(x: CGFloat(section.offset(for: geometry)), y: 0)
                }
                // current position
                RoundedRectangle(cornerRadius: 3)
                    .stroke()
                    .fill(Color.secondary)
                    .frame(width: 40, height: 30)
                    .overlay(Rectangle()
                                .fill(Color.secondary).frame(width: 2, height: 20))
                    .offset(x: abs(geometry.size.width * CGFloat(self.player.currentPosition)) - 20, y: 0)
            }
            // gesture
            .overlay(TapGestureView { p in
                guard let section = self.detectSection(from: p, in: geometry) else { return }
                self.select(section)
            })
        }.frame(height: 28)
        // current position needs some extra space
        .padding(.horizontal, 20)
    }
    
    /// Detect nearest video section for given point
    /// - Parameters:
    ///   - point: Point
    ///   - geometry: Geometry of superview
    /// - Returns: Nearest video section if exists
    func detectSection(from point: CGPoint, in geometry: GeometryProxy) -> VideoSection? {
        // normalize x value
        let norm = Double(point.x / geometry.size.width)
        // get all distances
        let distances = player.video.sections.map {
            ($0, min(abs($0.start - norm), abs(norm - $0.end)))
        }
        // select nearest
        return distances.sorted { $0.1 < $1.1 }.first?.0
    }
    
    func select(_ section: VideoSection) {
        player.selectedSection = section
    }
}

struct VideoProgressView_Previews: PreviewProvider {
    static var previews: some View {
        let v = VideoDatabase.shared.videos.first!
        let vp = VideoPlayer(video: v)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            ServerBackend().downloadSections(for: v)
        }
        return VideoProgressView(player: vp)
            .environment(\.colorScheme, .dark)
            .background(Color.black)
            .padding()
    }
}

// MARK: - Video section normalizations
extension VideoSection {
    func offset(for proxy: GeometryProxy) -> Double {
        Double(proxy.size.width) * start
    }
    
    func width(for proxy: GeometryProxy) -> CGFloat {
        precondition(end > start)
        return proxy.size.width * CGFloat(end - start)
    }
}
